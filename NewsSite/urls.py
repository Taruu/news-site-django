from django.contrib import admin
from django.urls import path
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView)
from NewsSite.views import NewsView, NewsEdit, register, SendFeedback

urlpatterns = [
    path('admin/', admin.site.urls),
    # Path to obtain a new access and refresh token
    path('api/token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    # Submit your refresh token to this path to obtain a new access token
    path('api/token/refresh/', TokenRefreshView.as_view(),
         name='token_refresh'),
    # Return 'Mods' model objects
    path('api/news/<int:page>/', NewsView.as_view(), name='news_view'),
    path('api/news/edit/', NewsEdit.as_view(), name='news_edit'),
    # Register a new user
    path('api/register/', register, name='register_view'),
    path('api/feedback/', SendFeedback.as_view(), name='feedback_method'),
]
