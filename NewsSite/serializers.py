from rest_framework import serializers
from NewsSite.models import News


# output serializer class for  'Mods' model
class NewsSerializer(serializers.ModelSerializer):
    owner = serializers.StringRelatedField()
    class Meta:
        model = News
        fields = '__all__'

