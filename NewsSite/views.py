from rest_framework.permissions import IsAuthenticated
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework import generics
from NewsSite.models import News
from NewsSite.serializers import NewsSerializer
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view
from django.contrib.auth.models import User
import json
from django.contrib.auth.hashers import make_password
from django.db.utils import IntegrityError
from django.utils import timezone
from django.core.mail import send_mail
from django.conf import settings


# View for 'Mods' model
class NewsView(generics.RetrieveAPIView):
    permission_classes = [AllowAny]

    def get(self, request, *args, **kwargs):
        page = kwargs["page"]
        start = page * 12
        end = start + 12
        max_page = (News.objects.count() // 12) + 1
        queryset = News.objects.order_by('-created_at').all()[start:end]
        serializer = NewsSerializer(queryset, many=True)
        json = {"now_page": serializer.data, "max_page": max_page}
        return Response(json)

    def post(self, request, *args, **kwargs):
        json_data = json.loads(request.body.decode())
        news_id = json_data["news_id"]
        news = News.objects.get(pk=news_id)
        serialized = NewsSerializer(news)
        return Response(serialized.data)


class SendFeedback(generics.CreateAPIView):
    permission_classes = [AllowAny]

    def post(self, request, *args, **kwargs):
        json_data = json.loads(request.body.decode())
        email = json_data["email"]
        name = json_data["name"]
        text_feedback = json_data["feedback"]
        send_mail(f"Feedback from {name} ({email})", text_feedback, email,
                  settings.EMAIL_ADMINS)
        return Response({"send": True})


class NewsEdit(generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        json_data = json.loads(request.body.decode())
        name = json_data["name"]
        text = json_data["content"]
        user = request.user
        new_news = News(name=name, content=text, owner=user)
        new_news.save()
        serialized = NewsSerializer(new_news)
        return Response(serialized.data)

    def put(self, request, *args, **kwargs):
        json_data = json.loads(request.body.decode())
        name = json_data["name"]
        text = json_data["content"]
        user = request.user
        news_id = json_data["news_id"]

        news = News.objects.get(pk=news_id)
        if news.owner.id != user.id:
            return Response({"error": "not access"})
        if news.name != name:
            news.name = name
        if news.content != text:
            news.content = text
        news.updated_at = timezone.now()
        news.save()
        serialized = NewsSerializer(news)
        return Response(serialized.data)

    def delete(self, request, *args, **kwargs):
        json_data = json.loads(request.body.decode())
        user = request.user
        news_id = json_data["news_id"]
        news = News.objects.get(pk=news_id)
        if news.owner.id != user.id:
            return Response({"error": "not access"})
        news.delete()
        return Response({"delete": True})


@csrf_exempt
@api_view(['GET', 'POST'])
def register(request):
    body = json.loads(request.body)
    username = body["username"]
    email = body["email"]
    password = body["password"]  # make_password()

    if body['password'] == body['confirm']:
        try:
            user = User.objects.create_user(username=username, email=email,
                                            password=password, is_active=True)
            user.save()
            return Response({"register": True}, status=202)
        except IntegrityError:
            return Response({"register": False}, status=401)
    else:
        return Response({"register": False}, status=401)
